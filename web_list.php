<?php
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <style>
    body {
        font-family: sans-serif;
        padding: 10px;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .search-form {
        display: flex;
        flex-direction: column;
        width: 40%;
        padding: 10px 30px;
    }

    input[type="text"] {
        width: 100%;
        border: solid 2px #007bc7;
        outline: none;
        padding: 8px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
        width: 100%;
        padding: 8px;
    }

    input[type="submit"] {
        padding: 12px 30px;
        background-color: #1de416;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 1em;
    }

    td,
    th {
        /* border: 1px solid #dddddd; */
        text-align: left;
        padding: 6px;
    }

    form td {
        text-align: center;
    }

    .wrap-action {
        width: 90%;
        display: flex;
        justify-content: end;
    }

    .action {
        color: white;
        display: inline-block;
        padding: 10px;
        background-color: #1de416;
        border: 1px solid #007bc7;
        text-decoration: none;
        border-radius: 10px;
        margin-right: 8px;
    }
    </style>
</head>

<body>
    <div class="search-form">
        <form name="registerForm" method="GET" enctype="multipart/form-data" action="">
            <table>
            <tr height = '40px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Phân Khoa</label>
                </td>
                <td height = '40px'>
                    <select name='faculty' style = 'border-color:#1de416;
                                                    height: 100%; width: 80%;'>
                        <?php
                            $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($facultyArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height = '40px'>
            <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Từ khoá</label>
                </td>
                <td height = '40px'>
                    <input name="keyword" type="text" style = 'border-color:#1de416;
                                                    height: 100%; width: 80%;'>
                </td>
            </tr>
            </table>
            <button style='background-color: #1de416; border-radius: 10px; 
                    width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Tìm kiếm</button>
        </form>
    </div>
    <div class="result-search">
        <span>Số sinh viên tìm thấy:</span>
        <span>XXX</span>
    </div>
    <div class="list-student">
        <div class="wrap-action">
            <a class="action" href="./web_register.php">Thêm</a>
        </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>