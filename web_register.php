<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Register</title>
<body>
    <fieldset style='width: 600px; height: 600px; border:#1de416 solid'>
    <?php
    session_start();
    date_default_timezone_set('Asia/Ho_Chi_Minh');

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $check = true;
            if(empty(handler($_POST["name"]))){
                echo "<div style='color: red;'>Hãy nhập tên</div>";
                $check=false;
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
                $check=false;
            }
            if(empty(handler($_POST["faculty"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
                $check=false;
            }
            $birthOfDate = handler($_POST["dob"]);
            if(empty($birthOfDate)){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
                $check=false;
            }
            elseif (!validateDate($birthOfDate)) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
                $check=false;
            }
            if (!isset($_FILES["fileUpload"])) {
                echo "Dữ liệu ảnh không đúng định dạng";
            }
            if ($_FILES["fileUpload"]['error'] != 0) {
                echo "Dữ liệu ảnh bị lỗi";
            }    
            if ($check) {
                $target_directory = "uploads/";
                $new_name = "uploads/".$_FILES["fileUpload"]["name"]."_".date("YmdHis").".";

                $target_file  = $target_directory.basename($_FILES["fileUpload"]["name"]);
                $allowUpload   = true;

                $imageExt = pathinfo($target_file, PATHINFO_EXTENSION);
                $maxFileSize = 800000; // In bytes

                $allowExts = array('jpg', 'png', 'jpeg', 'gif');
                $check = getimagesize($_FILES["fileUpload"]["tmp_name"]);

                if($check == false) {
                    echo "<div style='color: red;'>Không phải file ảnh</div>";
                    $allowUpload = false;
                } 
                // Check if file has already existed and not allow overwrite
                if (file_exists($target_file)) {
                    echo "<div style='color: red;'> File đã tồn tại trên server.</div>";
                    $allowUpload = false;
                }
                // Check if file size has exceeded allowed size
                if ($_FILES["fileUpload"]["size"] > $maxFileSize) {
                    echo "<div style='color: red;'> File vượt quá kích thước cho phép ($maxFileSize bytes).</div>";
                    $allowUpload = false;
                }
                // Check file extension
                if (!in_array($imageExt,$allowExts )) {
                    echo "<div style='color: red;'> Chỉ được upload ảnh định dạng JPG, PNG, JPEG, GIF</div>";
                    $allowUpload = false;
                }

                $fileExt = explode('.', $_FILES["fileUpload"]["name"]);
                
                $fileActualExt = strtolower(array_pop($fileExt));
                
                $fileName = date('YmdHis');
                $new_name =  $target_directory.str_replace(' ', '', join('.', $fileExt)) . "_" . $fileName . "." . $fileActualExt;

                echo $new_name;
                if ($allowUpload==true && $check==true) 
                {
                    if (move_uploaded_file( $_FILES["fileUpload"]["tmp_name"],$new_name)) {
                        // do something
                    }
                    else {
                        echo "<div style='color: red;'>Có lỗi xảy ra khi upload file.</div>";
                        $allowUpload= false;
                    }
                } 
            }  

            if ($allowUpload == true && $check == true) {
                $_SESSION = $_POST;
                $_SESSION["filename"] = $new_name;
                header("location: web_confirm.php");
            }          
        }
            
        function handler($data) {
            $data = stripslashes($data);
            $data = trim($data);
            return $data;
        }

        function validateDate($date) {
            // validate MM/DD/YYYY
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
                return true;
            } else {
                return false;
            }
        }   
    ?>
    <form style='margin: 50px 70px 0 50px' method="post" enctype="multipart/form-data">
        <table style = 'border-collapse: separate; border-spacing: 15px 15px;'>
            <tr height = '40px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Họ và tên</label>
                    <span style='color: red;'>*</span>
                </td>
                <td >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#1de416'>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 40% style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Giới tính</label>
                </td>
                <td width = 40% >
                <?php
                    $genderArr=array("Nam","Nữ");
                    for($x = 0; $x < count($genderArr); $x++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                .$genderArr[$x]. 
                            "</label>";
                    }
                ?>  
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Phân Khoa</label>
                    <span style='color: red;'>*</span>
                </td>
                <td height = '40px'>
                    <select name='faculty' style = 'border-color:#1de416;
                                                    height: 100%; width: 80%;'>
                        <?php
                            $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($facultyArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Ngày sinh</label>
                    <span style='color: red;'>*</span>
                </td>
                <td height = '40px'>
                    <input type='date' name="dob" data-date="" data-date-format="YYYY-MM-DD" style = 'line-height: 32px; border-color:#1de416'>
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'line-height: 34px; border-color:#1de416'> 
                </td>
            </tr>

            <tr height = '100px'>
                <td style = 'background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Hình ảnh</label>
                </td>
                <td height = '100px'>
                    <input type="file" name="fileUpload" id="myFile">
                </td>
            </tr>

        </table>
        <button style='background-color: #1de416; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
    </form>

</fieldset>
</body>
</html>